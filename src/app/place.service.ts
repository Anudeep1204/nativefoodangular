import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class PlaceService {

  private baseUrl = '//localhost:8081/api/';

  constructor(private http: HttpClient) { }

    createPlace(place: Object): Observable<Object> {
      return this.http.post(this.baseUrl+`places/create`, place);
    }

    getAllPlaces():Observable<any> {
      return this.http.get(this.baseUrl+'places');
    }
  
    getOnePlace(placeId: string){
      return this.http.get(this.baseUrl+'places/'+placeId);
    }
  
    editOnePlace(placeId: string, place: Object):Observable<any>{
      return this.http.put(this.baseUrl+'places/'+placeId, place);
    }
  
    deleteOnePlace(placeId: string){
      return this.http.delete(this.baseUrl+'places/'+placeId);
    }
  
    deleteAllPlace(){
      return this.http.delete(this.baseUrl+'places/deleteAll');
    } 
  
}