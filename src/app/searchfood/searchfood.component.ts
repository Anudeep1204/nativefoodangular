import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FoodService } from '../food.service';
import { FormGroup, FormArray, FormControl, FormBuilder, NgForm, Validators, ValidatorFn  } from '@angular/forms';
import { Router } from '@angular/router';
import { Food } from '../food';

@Component({
  selector: 'app-searchfood',
  templateUrl: './searchfood.component.html',
  styleUrls: ['./searchfood.component.css']
})
export class SearchfoodComponent implements OnInit {
    locationName:string="";
    sweetName:string="";
    sweetId: string="";

    foodRegistration:FormGroup;

    foods: Array<any>;
    showNew: Boolean = false;
    selectedRow: number;
    food: Food;

  constructor(private http: HttpClient, private foodService: FoodService, private frmbuilder:FormBuilder, private router:Router) {
    this.foodRegistration=frmbuilder.group({
      locationName:['',Validators.compose([Validators.required,Validators.pattern('[a-zA-Z ][a-zA-Z ]+')])],
      sweetName:['',Validators.compose([Validators.required,Validators.pattern('[a-zA-Z ][a-zA-Z ]+')])]
    });
   }

  ngOnInit() {
    this.foodService.getAllFoods().subscribe(data =>{
      this.foods = data;
    })
  }

  onNew() {
    /* this.showNew = true; */
    this.router.navigate(['/foodregister']);
  } 
 
  onEdit(index: number,food,sweetId){
    this.showNew = true;
    this.selectedRow = index;
    this.food=food;
    this.food.sweetId=sweetId;
  }

  editFood(sweetId,food){
    this.sweetId=sweetId;
    this.food=food;
    this.foodService.editOneFood(sweetId,food)
    .subscribe(data =>{
      this.router.navigate(['/searchfood',data]);
    }, error => console.log(error));
    this.showNew = false;
  }

  onDelete(index: number,sweetId){
    this.foodService.deleteOneFood(sweetId)
    .subscribe(()=>{
      this.foods.splice(index, 1);
      console.log(sweetId);
    });
  }

  onDeleteAll(){
    this.foodService.deleteAllFood()
      .subscribe(()=>{this.router.navigate(['/foodregister',]);
    },error => console.log(error));  
     this.router.navigate(['/foodregister']); 
  } 

  onCancel() {
    this.showNew = false;
  }

}
