import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodregisterComponent } from './foodregister.component';

describe('FoodregisterComponent', () => {
  let component: FoodregisterComponent;
  let fixture: ComponentFixture<FoodregisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoodregisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodregisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
