import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormControl, FormBuilder, NgForm, Validators, ValidatorFn  } from '@angular/forms';
import { Router } from '@angular/router';
import { Food } from '../food';
import { FoodService } from '../food.service';


@Component({
  selector: 'app-foodregister',
  templateUrl: './foodregister.component.html',
  styleUrls: ['./foodregister.component.css']
})
export class FoodregisterComponent implements OnInit {
  locationName: string;
  sweetName: string;

  foodRegistration:FormGroup;

  food: Food = new Food();

  constructor(private frmbuilder:FormBuilder, private router:Router,private foodService: FoodService) {
    this.foodRegistration=frmbuilder.group({
      locationName:['',Validators.compose([Validators.required,Validators.pattern('[a-zA-Z ][a-zA-Z ]+')])],
      sweetName:['',Validators.compose([Validators.required,Validators.pattern('[a-zA-Z ][a-zA-Z ]+')])]
    });
   }

  ngOnInit() {
  }

  saveFood(){
    this.foodService.create(this.food)
   .subscribe(data => {
     this.router.navigate(['/searchfood',data]);
   }, error => console.log(error));
   this.food = new Food();    
  } 

}
