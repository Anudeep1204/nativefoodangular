import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class FoodService {

  private baseUrl = '//localhost:8081/api/';

  constructor(private http: HttpClient) { }

  create(food: Object): Observable<Object> {
    return this.http.post(this.baseUrl+`sweets/create`, food);
  }

  getAllFoods():Observable<any> {
    return this.http.get(this.baseUrl+'sweets');
  }

  getOneFood(sweetId: string){
    return this.http.get(this.baseUrl+'sweets/'+sweetId);
  }

  editOneFood(sweetId: string, food: Object):Observable<any>{
    return this.http.put(this.baseUrl+'sweets/'+sweetId, food);
  }

  deleteOneFood(sweetId: string){
    return this.http.delete(this.baseUrl+'sweets/'+sweetId);
  }

  deleteAllFood(){
    return this.http.delete(this.baseUrl+'sweets/delete');
  }

}
