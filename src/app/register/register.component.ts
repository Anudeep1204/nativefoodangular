import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormControl, FormBuilder, NgForm, Validators, ValidatorFn  } from '@angular/forms';
import { Router } from '@angular/router';
import { Vendor } from '../vendor';
import { VendorService } from '../vendor.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
    vendorName:string="";
    vendorStoreName:string="";
    vendorStoreCity:string="";
    vendorEmail:string="";
    services: Array<any>=[];

    /* services={
      servicetypes:
      [
        {name: 'Breakfast', id:'Breakfast', selected:false},
        {name: 'Lunch/Dinner', id:'Lunch/Dinner', selected:false},
        {name: 'Bakery', id:'Bakery', selected:false},
        {name: 'Sweets', id:'Sweets', selected:false},
      ]  
    } */
     
    emailPattern:string="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$";
    vendorRegistration:FormGroup;

    vendor: Vendor = new Vendor();

  constructor(private frmbuilder:FormBuilder, private router:Router,private vendorService: VendorService) {

    this.vendorRegistration=frmbuilder.group({
      vendorName:['',Validators.compose([Validators.required,Validators.pattern('[a-zA-Z ][a-zA-Z ]+')])],
      vendorStoreName:['',Validators.compose([Validators.required,Validators.pattern('[a-zA-Z ][a-zA-Z ]+')])],
      vendorStoreCity:['',Validators.compose([Validators.required])],
      vendorEmail:['',Validators.compose([Validators.required,Validators.pattern(this.emailPattern)])],
      services:['',Validators.compose([Validators.required])]
   /*    servicetypes: new FormArray(this.services.servicetypes.map(servicetype => new FormControl(servicetype)),Validators.minLength(1))
    */ });
   }

  ngOnInit() {
  }

/*   changeServiceTypes(servicetype: any) {
    var currentServiceTypeControls: FormArray = this.vendorRegistration.get('servicetype') as FormArray;
    var index = currentServiceTypeControls.value.indexOf(servicetype);
    if(index > -1) currentServiceTypeControls.removeAt(index) //If the user currently uses this schedule, remove it.
    else currentServiceTypeControls.push(new FormControl(servicetype)); //Otherwise add this schedule.
} */

  saveVendor(){
     this.vendorService.createVendor(this.vendor)
    .subscribe(data => {
      this.router.navigate(['/search',data]);
    }, error => console.log(error));
    this.vendor = new Vendor();    
  }
}