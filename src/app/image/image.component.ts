import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Product } from '../product';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent implements OnInit {
  productName: string="";
  productType: string="";
  availabilityLocation: string="";
  productDescription: string="";

  products: Array<any>;
  product: Product;

  constructor(private http: HttpClient, private productService: ProductService, private router:Router) { }

  ngOnInit() {
    this.productService.getAllProducts().subscribe(data =>{
      this.products = data;
    })
  }

}
