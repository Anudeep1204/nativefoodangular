import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormControl, FormBuilder, NgForm, Validators, ValidatorFn  } from '@angular/forms';
import { Router } from '@angular/router';
import { Product } from '../product';
import { ProductService } from '../product.service';
import { HttpClient, HttpResponse, HttpEventType } from '@angular/common/http';

@Component({
  selector: 'app-deployproduct',
  templateUrl: './deployproduct.component.html',
  styleUrls: ['./deployproduct.component.css']
})
export class DeployproductComponent implements OnInit {
  productName: string="";
  productType: string="";
  availabilityLocation: string="";
  productDescription: string="";

  selectedFiles: FileList;
  currentFileUpload: File;
  progress: { percentage: number } = { percentage: 0 };
 

  deployProducts:FormGroup;

  showSingle: Boolean = false;
  showMultiple: Boolean = false;
  showMultipleForm: Boolean = false;

  product: Product = new Product();

  constructor(private formBuilder:FormBuilder, private router:Router,private deployProductService: ProductService) {
    this.deployProducts=formBuilder.group({
      productName:['',Validators.compose([Validators.required,Validators.pattern('[a-zA-Z][a-zA-Z]+')])],
      productType:['',Validators.compose([Validators.required,Validators.pattern('[a-zA-Z][a-zA-Z]+')])],
      availabilityLocation:['',Validators.compose([Validators.required,Validators.pattern('[a-zA-Z][a-zA-Z]+')])],
      productDescription: new FormControl('',Validators.compose([Validators.maxLength(150)]))/* ,Validators.pattern('[a-zA-Z][a-zA-Z.,!"\s]+') */
      });
   }

  ngOnInit() {
  }

  selectFile(event) {
    const file = event.target.files.item(0);

    if (file.type.match('image.*')) {
      this.selectedFiles = event.target.files;
    } else {
      alert('invalid format!');
    }
  }

  upload() {
    this.progress.percentage = 0;

    this.currentFileUpload = this.selectedFiles.item(0);
    this.deployProductService.fileUpload(this.currentFileUpload).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress.percentage = Math.round(100 * event.loaded / event.total);
      } else if (event instanceof HttpResponse) {
        console.log('File is completely uploaded!');
      }
    });

    this.selectedFiles = undefined;
  }

  Single(){
    this.showSingle=true;
    this.showMultiple=false;
    this.showMultipleForm=false;
  }

  Multiple(){
    this.showMultiple=true;
    this.showSingle=false;
    this.showMultipleForm=false;
  }

  MultipleForm(){
    this.showMultipleForm=true;
  }

  deployProduct(){
    this.deployProductService.insertProduct(this.product)
    .subscribe(data => { console.log(data)
    },error => console.log(error)
    );
    this.product = new Product(); 
  }

}
