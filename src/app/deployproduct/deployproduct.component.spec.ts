import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeployproductComponent } from './deployproduct.component';

describe('DeployproductComponent', () => {
  let component: DeployproductComponent;
  let fixture: ComponentFixture<DeployproductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeployproductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeployproductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
