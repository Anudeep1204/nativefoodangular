import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private imageUrl = '//localhost:8082/productapi/';

  constructor(private http: HttpClient) { }

  fileUpload(file: File): Observable<HttpEvent<{}>> {
    const formdata: FormData = new FormData();

    formdata.append('file', file);

    const req = new HttpRequest('POST', this.imageUrl+'post', formdata, {
      reportProgress: true,
      responseType: 'text'
    });

    return this.http.request(req);
  }


  insertProduct(product: Object): Observable<Object> {
    return this.http.post(this.imageUrl+`products/create`, product);
  }

  getAllProducts():Observable<any> {
    return this.http.get(this.imageUrl+'products');
  }

  getOneProduct(productId: string){
    return this.http.get(this.imageUrl+'products/'+productId);
  }

  editOneProduct(productId: string, product: Object):Observable<any>{
    return this.http.put(this.imageUrl+'products/'+productId, product);
  }

  deleteOneProduct(productId: string){
    return this.http.delete(this.imageUrl+'products/'+productId);
  }

  deleteAllProducts(){
    return this.http.delete(this.imageUrl+'products/delete');
  }
  
}
