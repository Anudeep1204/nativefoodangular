import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PlaceService } from '../place.service';
import { FormGroup, FormArray, FormControl, FormBuilder, NgForm, Validators, ValidatorFn  } from '@angular/forms';
import { Router } from '@angular/router';
import { Place } from '../place';

@Component({
  selector: 'app-searchplace',
  templateUrl: './searchplace.component.html',
  styleUrls: ['./searchplace.component.css']
})
export class SearchplaceComponent implements OnInit {
    placeName:string="";
    state:string="";
    country:string="";
    pincode:number=null;
    placeId: string="";

    placeRegistration:FormGroup;

    places: Array<any>;
    showNew: Boolean = false;
    selectedRow: number;
    place: Place;

  constructor(private http: HttpClient, private placeService: PlaceService, private frmbuilder:FormBuilder, private router:Router) { 
    this.placeRegistration=frmbuilder.group({
      placeName:['',Validators.compose([Validators.required,Validators.pattern('[a-zA-Z ][a-zA-Z ]+')])],
      state:['',Validators.compose([Validators.required])],
      country:['',Validators.compose([Validators.required])],
      pincode:['',Validators.compose([Validators.required,Validators.pattern('[0-9]{6}')])]
    });
  }

  ngOnInit() {
    this.placeService.getAllPlaces().subscribe(data =>{
      this.places = data;
    })
  }

  onNew() {
    /* this.showNew = true; */
    this.router.navigate(['/placeregister']);
  } 
 
  onEdit(index: number,place,placeId){
    this.showNew = true;
    this.selectedRow = index;
    this.place=place;
    this.place.placeId=placeId;
  }

  editPlace(placeId,place){
    this.placeId=placeId;
    this.place=place;
    this.placeService.editOnePlace(placeId,place)
    .subscribe(data =>{
      this.router.navigate(['/searchplace',data]);
    }, error => console.log(error));
    this.showNew = false;
  }

  onDelete(index: number,placeId){
    this.placeService.deleteOnePlace(placeId)
    .subscribe(()=>{
      this.places.splice(index, 1);
      console.log(placeId);
    });
  }

  onDeleteAll(){
    this.placeService.deleteAllPlace()
      .subscribe(()=>{this.router.navigate(['/placeregister',]);
    },error => console.log(error));  
     this.router.navigate(['/placeregister']); 
  } 

  onCancel() {
    this.showNew = false;
  }

}