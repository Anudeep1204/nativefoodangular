import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA  } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { RegisterComponent } from './register/register.component';
import { SearchComponent } from './search/search.component';
import { HomeComponent } from './home/home.component'; 
import { PlaceregisterComponent } from './placeregister/placeregister.component';
import { SearchplaceComponent } from './searchplace/searchplace.component';

import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { Ng2CarouselamosModule } from 'ng2-carouselamos';
import { FoodregisterComponent } from './foodregister/foodregister.component';
import { SearchfoodComponent } from './searchfood/searchfood.component';
import { DeployproductComponent } from './deployproduct/deployproduct.component';
import { ImageComponent } from './image/image.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    RegisterComponent,
    SearchComponent,
    HomeComponent,
    PlaceregisterComponent,
    SearchplaceComponent,
    FoodregisterComponent,
    SearchfoodComponent,
    DeployproductComponent,
    ImageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    Ng2CarouselamosModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
