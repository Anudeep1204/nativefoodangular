import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VendorService {

  private baseUrl = '//localhost:8081/api/';
  
  constructor(private http: HttpClient) { }

  createVendor(vendor: Object): Observable<Object> {
    return this.http.post(this.baseUrl+`vendors/create`, vendor);
  }

  getAllVendor():Observable<any> {
    return this.http.get(this.baseUrl+'vendors');
  }

  getOneVendor(vendorId: string){
    return this.http.get(this.baseUrl+'vendors/'+vendorId);
  }

  editOneVendor(vendorId: string, vendor: Object):Observable<any>{
    return this.http.put(this.baseUrl+'vendors/'+vendorId, vendor);
  }

  deleteOneVendor(vendorId: string){
    return this.http.delete(this.baseUrl+'vendors/'+vendorId);
  }

  deleteAllVendor(){
    return this.http.delete(this.baseUrl+'vendors/delete');
  }
   
}