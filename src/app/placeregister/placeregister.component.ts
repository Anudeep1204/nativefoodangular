import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormControl, FormBuilder, NgForm, Validators, ValidatorFn  } from '@angular/forms';
import { Router } from '@angular/router';
import { Place } from '../place';
import { PlaceService } from '../place.service';

@Component({
  selector: 'app-placeregister',
  templateUrl: './placeregister.component.html',
  styleUrls: ['./placeregister.component.css']
})
export class PlaceregisterComponent implements OnInit {
  placeName: string;
  state: string;
	country: string;
  pincode: number;
  
  placeRegistration:FormGroup;

  place: Place = new Place();

  constructor(private frmbuilder:FormBuilder, private router:Router,private placeService: PlaceService) { 

    this.placeRegistration=frmbuilder.group({
      placeName:['',Validators.compose([Validators.required,Validators.pattern('[a-zA-Z ][a-zA-Z ]+')])],
      state:['',Validators.compose([Validators.required])],
      country:['',Validators.compose([Validators.required])],
      pincode:['',Validators.compose([Validators.required,Validators.pattern('[0-9]{6}')])]
    });
  }

  ngOnInit() {
  }

  savePlace(){
    this.placeService.createPlace(this.place)
   .subscribe(data => {
     this.router.navigate(['/searchplace',data]);
   }, error => console.log(error));
   this.place = new Place();    
 }

}
