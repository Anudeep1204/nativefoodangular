import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaceregisterComponent } from './placeregister.component';

describe('PlaceregisterComponent', () => {
  let component: PlaceregisterComponent;
  let fixture: ComponentFixture<PlaceregisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaceregisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaceregisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
