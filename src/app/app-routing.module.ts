import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { SearchComponent } from './search/search.component';
import { HomeComponent } from './home/home.component';
import { PlaceregisterComponent } from './placeregister/placeregister.component';
import { SearchplaceComponent } from './searchplace/searchplace.component';
import { SearchfoodComponent } from './searchfood/searchfood.component';

import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FoodregisterComponent } from './foodregister/foodregister.component';
import { DeployproductComponent } from './deployproduct/deployproduct.component';
import { ImageComponent } from './image/image.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'placeregister',
    component: PlaceregisterComponent
  },
  {
    path: 'foodregister',
    component: FoodregisterComponent
  },
  {
    path: 'search',
    component: SearchComponent
  },
  {
    path: 'searchplace',
    component: SearchplaceComponent
  },
  {
    path: 'searchfood',
    component: SearchfoodComponent
  },
  {
    path: 'deployproduct',
    component: DeployproductComponent
  },
  {
    path: 'displayproduct',
    component: ImageComponent
  }
];

@NgModule({
  imports: [FormsModule,HttpClientModule,RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 
  
}
