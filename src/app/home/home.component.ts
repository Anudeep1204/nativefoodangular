import { Component, OnInit } from '@angular/core';
/* import { FormBuilder, FormGroup, FormArray, FormControl, ValidatorFn } from '@angular/forms';
 */

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  items: Array<any> = [];
  /* form: FormGroup;
  orders = [
    { id: 100, name: 'order 1' },
    { id: 200, name: 'order 2' },
    { id: 300, name: 'order 3' },
    { id: 400, name: 'order 4' }
  ]; */
  constructor(/* private formBuilder: FormBuilder */) {
    /* const controls = this.orders.map(c => new FormControl(false));
    controls[0].setValue(true);

    this.form = this.formBuilder.group({
      orders: new FormArray(controls, minSelectedCheckboxes(1))
    }); */
    this.items = [
      { name: '../../assets/images/1.jpeg' },
      { name: '../../assets/images/2.jpeg' },
      { name: '../../assets/images/3.jpeg' }
    ]
   }

  ngOnInit() {
  }
  /* submit() {
    const selectedOrderIds = this.form.value.orders
      .map((v, i) => v ? this.orders[i].id : null)
      .filter(v => v !== null);

    console.log(selectedOrderIds);
  } */
}

/* function minSelectedCheckboxes(min = 1) {
  const validator: ValidatorFn = (formArray: FormArray) => {
    const totalSelected = formArray.controls
      .map(control => control.value)
      .reduce((prev, next) => next ? prev + next : prev, 0);

    return totalSelected >= min ? null : { required: true };
  };

  return validator;
} */