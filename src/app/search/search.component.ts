import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { VendorService } from '../vendor.service';
import { FormGroup, FormArray, FormControl, FormBuilder, NgForm, Validators, ValidatorFn  } from '@angular/forms';
import { Router } from '@angular/router';
import { Vendor } from '../vendor';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
    vendorName:string="";
    vendorStoreName:string="";
    vendorStoreCity:string="";
    vendorEmail:string="";
    services: string[]=[];
    vendorId: string="";
  
  vendorRegistration:FormGroup;
  emailPattern:string="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$";

   vendors: Array<any>;
   showNew: Boolean = false;
   selectedRow: number;
   vendor: Vendor;

   constructor(private http: HttpClient, private vendorService: VendorService, private frmbuilder:FormBuilder, private router:Router) {
    this.vendorRegistration=frmbuilder.group({
      vendorName:['',Validators.compose([Validators.required,Validators.pattern('[a-zA-Z ][a-zA-Z ]+')])],
      vendorStoreName:['',Validators.compose([Validators.required,Validators.pattern('[a-zA-Z ][a-zA-Z ]+')])],
      vendorStoreCity:['',Validators.compose([Validators.required])],
      vendorEmail:['',Validators.compose([Validators.required,Validators.pattern(this.emailPattern)])],
      services:['',Validators.compose([Validators.required])]
    });
   }
  ngOnInit() {
    this.vendorService.getAllVendor().subscribe(data =>{
      this.vendors = data;
    })
  }

   onNew() {
    /* this.showNew = true; */
    this.router.navigate(['/register']);
  } 

  onEdit(index: number,vendor,vendorId){
    this.showNew = true;
    this.selectedRow = index;
    this.vendor=vendor;
    this.vendor.vendorId=vendorId;
  }

  editVendor(vendorId,vendor){
    this.vendorId=vendorId;
    this.vendor=vendor;
    this.vendorService.editOneVendor(vendorId,vendor)
    .subscribe(data =>{
      this.router.navigate(['/search',data]);
    }, error => console.log(error));
    this.showNew = false;
  }

  onDelete(index: number,vendorId){
    this.vendorService.deleteOneVendor(vendorId)
    .subscribe(()=>{
      this.vendors.splice(index, 1);
      console.log(vendorId);
    });
  }

  onDeleteAll(){
    this.vendorService.deleteAllVendor()
      .subscribe(()=>{this.router.navigate(['/register',]);
    },error => console.log(error));  
     this.router.navigate(['/register']); 
  }

  onCancel() {
    this.showNew = false;
  }

}