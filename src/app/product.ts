export class Product {
    productName: string;
    productType: string;
    availabilityLocation: string;
    productDescription: string;
}