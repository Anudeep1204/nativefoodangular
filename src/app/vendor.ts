export class Vendor {
	vendorId: string;
	vendorName: string;
	vendorStoreName: string;
	vendorStoreCity: string;
	vendorEmail: string;
	services: Array<any>=[];
}